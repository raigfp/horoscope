import random
import re
from data import ZODIACS, TEMPLATES, SUBJECTS


def get_random_item(data, zodiac, date):
    x = int(date.replace('-', '') + str(ZODIACS.index(zodiac)))
    random.seed(x)
    return data[random.randint(0, len(data) - 1)]


def generate(zodiac, date):
    return generate_sentence(
        get_random_item(SUBJECTS, zodiac, date),
        get_random_item(TEMPLATES, zodiac, date),
        zodiac,
        date
    )


def generate_sentence(subject, template, zodiac, date):
    return re.sub(
        '{{([^\s]*)}}',
        lambda match: replace_phrase(match.group(1), subject, zodiac, date),
        template,
        flags=re.DOTALL
    ).capitalize()


def replace_phrase(phrase, subject, zodiac, date):
    return get_random_item(subject[phrase], zodiac, date)

