from bottle import Bottle, template
from datetime import date
import horoscope

app = Bottle()


@app.get('/favicon.ico')
def get_favicon():
    return


@app.route('/<zodiac>')
@app.route('/<zodiac>/<horoscope_date>')
def index(zodiac, horoscope_date=date.today().isoformat()):
    return template(
            'horoscope',
            zodiac=zodiac.title(),
            date=horoscope_date,
            body=horoscope.generate(zodiac.lower(), horoscope_date)
    )


app.run(host='localhost', port=7000)
